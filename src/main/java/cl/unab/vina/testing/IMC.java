package cl.unab.vina.testing;

public class IMC {
	private float estatura;
	private float peso;
	private float imc;
	
	public IMC (float estatura, float peso) {
		this.estatura = estatura;
		this.peso = peso;		
	}

	public float getEstatura() {
		return estatura;
	}

	public void setEstatura(float estatura) {
		this.estatura = estatura;
	}

	public float getPeso() {
		return peso;
	}

	public void setPeso(float peso) {
		this.peso = peso;
	}
	
	
	public float getIMC() {
		if(this.peso != 0 && this.estatura != 0) {
			this.imc = (this.peso/ this.estatura*this.estatura);
		}
		return this.imc;
	}
	

}
