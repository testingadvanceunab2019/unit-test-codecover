package unit_test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import cl.unab.vina.testing.IMC;


public class TestIMC{
	float resultadoCorrecto;
	float resultadoIncorrecto;
	IMC imc;
	
	@Before
	public void setResult() {
		System.out.println("Las variables se ingresan antes, pero no el mensaje");
		imc = new IMC(1.65f, 78.0f);
		resultadoCorrecto = 78;
		resultadoIncorrecto = 100;
	}
	
	@BeforeClass
	public static void beforeClass() {
		System.out.println("Corre antes de ejecutarse la clase de pruebas");
	}
	
	@AfterClass
	public static void afterC() {
		System.out.println("Corre despues de ejecutarse la clase de pruebas");
	}

	@Test
	public void pruebaCorrecta() {
		assertEquals("Error Calculo IMC fue incorrecto",imc.getIMC(), resultadoCorrecto, 0);
		System.out.println("Prueba Correcta");
	}
	
	@Test
	public void pruebaIncorrecta() {
		assertNotEquals("Error Calculo IMC fue correcto",imc.getIMC(), resultadoIncorrecto, 0);
		System.out.println("Prueba Incorrecta");
	}


}
